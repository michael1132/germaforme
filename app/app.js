import Vue from "nativescript-vue";
import Home from "./components/Home";
import Page from "./components/Page";
import Profile from "./components/Profile";
import CreateAccount from "./components/CreateAccount";
import SelfInfo from "./components/SelfInfo";
import MapVertical from "./components/MapVertical";
import Nutrition from "./components/Nutrition";
import Run from "./components/Run";
import MapRunVertical from "./components/MapRunVertical";

new Vue({
    data () {
        return {

        }
    },
    template: `
        <Frame>
            <Home />
        </Frame>`,

    components: {
        Home,
        Page,
        Profile,
        CreateAccount,
        SelfInfo,
        MapVertical,
        Nutrition,
        Run,
        MapRunVertical,
    },
}).$start();
